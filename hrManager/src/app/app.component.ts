import { Component } from '@angular/core';
import { Anagrafica } from './model/anagrafica.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  anagrafica:Anagrafica;


  displayForm(anag:Anagrafica){
    this.anagrafica=anag;
  }


}


