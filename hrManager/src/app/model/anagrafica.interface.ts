export interface Anagrafica {
    idAnagrafica: number;
    cognome: string;
    nome: string;
    dataNascita: Date;
    sesso: string;
    telefono: string;
    mail: string;
    statoCivile: string;
    codFiscale?: string;
    iban: string;
    privacy: boolean;
    incensurato: boolean;
    dataIns: Date;
    userIns: string;
    dataUpd: Date;
    userUpd: string;
    dataNascitaString: string;
    dataInsString: string;
    dataUpdString: string;
}

