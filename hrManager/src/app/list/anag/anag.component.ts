import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Anagrafica } from 'src/app/model/anagrafica.interface';


@Component({
  selector: 'app-anag',
  templateUrl: './anag.component.html',
  styleUrls: ['./anag.component.css']
})
export class AnagComponent implements OnInit {

  @Input() anagrafica:Anagrafica;
  @Output() anagOut = new EventEmitter<Anagrafica>();

  constructor() { }

  ngOnInit() {
  }

  onSelectAnag(){
    this.anagOut.emit(this.anagrafica);
  }
}
