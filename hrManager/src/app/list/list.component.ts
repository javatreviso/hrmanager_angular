import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AnagraficaService } from '../anagrafica.service';
import { Anagrafica } from '../model/anagrafica.interface'
import { restoreView } from '@angular/core/src/render3';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  @Output() anagSelectedOut = new EventEmitter<Anagrafica>();

  anagraficaList: Anagrafica[];
  constructor(public rest: AnagraficaService) {
  }

  ngOnInit() {
    this.rest.getAnagraficaList().subscribe((list: Anagrafica[]) => {
        this.anagraficaList = list;
        console.log(this.anagraficaList);
    });
    
  }
  onSelectedAnag(anag:Anagrafica){
    this.anagSelectedOut.emit(anag);
  }

}
