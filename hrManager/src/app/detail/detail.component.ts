import { Component, OnInit, Input } from '@angular/core';
import { Anagrafica } from '../model/anagrafica.interface';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  @Input() anagrafica:Anagrafica;
  constructor() { }

  ngOnInit() {
  }

}
